import { component, React, Style } from 'local/react/component'
import { Tooltip } from 'local/react-ui/tooltip'

export const defaultAddressStyle: Style = {
  color: 'inherit',
  textDecoration: 'none',
  position: 'relative',
  display: 'inline-block'
}

export const defaultAddressLineStyle: Style = {
  display: 'block'
}

export const defaultTooltipText = 'Click for directions'

export const Address = component
  .props<{
    style?: Style
    lineStyle?: Style
    street?: string
    unit?: string
    city?: string
    state?: string
    zip?: string
    tooltipText?: string
  }>({
    style: defaultAddressStyle,
    lineStyle: defaultAddressLineStyle,
    tooltipText: defaultTooltipText
  })
  .render(
    ({ lineStyle, style, tooltipText, street, unit, city, state, zip }) => {
      const searchTerm = [street, unit, city, state, zip]
        .filter(x => x)
        .map(encodeURIComponent)
        .map(x => x.replace(/(%20)+/g, '+'))
        .join('+')

      return (
        <a
          style={style}
          target="_blank"
          href={`https://www.google.com/maps?q=${searchTerm}`}
        >
          <Tooltip text={tooltipText} />
          {street || unit ? (
            <span style={lineStyle}>
              {street}
              {street && unit ? ',' : ''} {unit}
            </span>
          ) : (
            undefined
          )}
          {city || state || zip ? (
            <span style={lineStyle}>
              {city}
              {city && state ? ',' : ''} {state} {zip}
            </span>
          ) : (
            undefined
          )}
        </a>
      )
    }
  )
